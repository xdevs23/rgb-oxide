# RGB Oxide

[WIP] The better:tm: RGB solution that uses the OpenRGB SDK

## How to use

Install OpenRGB (e. g. `openrgb-bin` on AUR).

Start OpenRGB server (make sure you don't have a service of OpenRGB running):
```bash
sudo openrgb --server --server-port 5000 --noautoconnect --autostart-disable
```

Clone this repo, then, inside of it:

```bash
cargo run --profile release
```

And follow on-screen instructions.

## License

```
    RGB Oxide – RGB client
    Copyright (C) 2023  Simão Gomes Viana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```