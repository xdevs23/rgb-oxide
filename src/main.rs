use std::error::Error;
use std::fmt::{Display, Formatter};
use colors_transform::{Color as RgbColor, Rgb};
use inquire::{Select, Text};
use inquire::validator::Validation;
use openrgb::data::{Color, Mode};
use openrgb::OpenRGB;

#[derive(Clone)]
struct ControllerInfo {
    id: u32,
    name: String,
}

impl Display for ControllerInfo {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.name.as_str())
    }
}

#[derive(Clone)]
struct ModeInfo {
    id: i32,
    name: String,
}

impl Display for ModeInfo {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.name.as_str())
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let client = OpenRGB::connect_to(("localhost", 5000)).await?;
    client.set_name("rgb-oxide").await?;

    let controllers = client.get_controller_count().await?;

    let mut info_controllers: Vec<ControllerInfo> = vec![];
    for controller_id in 0..controllers {
        let controller = client.get_controller(controller_id).await?;
        info_controllers.push(ControllerInfo { id: controller_id, name: controller.name });
    }

    let selected_controller =
        Select::new("Choose a controller", info_controllers).prompt().unwrap();

    let controller = client.get_controller(selected_controller.id).await?;

    let mut info_modes: Vec<ModeInfo> = vec![];
    let modes = &controller.modes;
    for mode_id in 0..modes.len() {
        info_modes.push(ModeInfo { id: mode_id as i32, name: modes[mode_id].name.clone() });
    }

    let selected_mode = Select::new("Choose mode", info_modes).prompt().unwrap();

    let hex_color_validator = |input: &str| -> Result<Validation, Box<dyn Error + Send + Sync>> {
        if Rgb::from_hex_str(input).is_ok() {
            Ok(Validation::Valid)
        } else {
            Ok(Validation::Invalid(format!("Invalid color {}", input).into()))
        }
    };

    let rgb_color = Rgb::from_hex_str(Text::new("New color (hex)")
        .with_validator(hex_color_validator).prompt().unwrap().as_str()).unwrap();

    let mode = &controller.modes[selected_mode.id as usize];
    client.update_mode(selected_controller.id, selected_mode.id, Mode {
        name: mode.name.clone(),
        value: mode.value,
        flags: mode.flags,
        speed_min: mode.speed_min,
        speed_max: mode.speed_max,
        speed: mode.speed,
        brightness_min: mode.brightness_min,
        brightness_max: mode.brightness_max,
        brightness: mode.brightness,
        color_mode: mode.color_mode,
        colors: mode.colors.clone(),
        colors_min: mode.colors_min,
        colors_max: mode.colors_max,
        direction: mode.direction
    }).await?;

    for led_id in 0..controller.leds.len() {
        client.update_led(
            selected_controller.id, led_id as i32, Color::from((
                rgb_color.get_red().round() as u8,
                rgb_color.get_green().round() as u8,
                rgb_color.get_blue().round() as u8,
            ))
        ).await?;
    }

    Ok(())
}
